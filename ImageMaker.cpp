#include "ImageMaker.h"
#include <iostream>

ImageMaker::ImageMaker()
{
        height = 0;
        width = 0;
        pen_red = 0;
        pen_green = 0;
        pen_blue = 0;
        for(int y=0; y < MAX_HEIGHT; y++) {
            for (int x = 0; x < MAX_WIDTH; x++) {
                image[y][x][RED] = 255;
                image[y][x][GREEN] = 255;
                image[y][x][BLUE] = 255;
            }
        }
}

ImageMaker::ImageMaker(string filename)
{
 ifstream ppmFile;
 ppmFile.open(filename);

 string magicNum, maxColorRange;
 ppmFile >> magicNum;
 if(magicNum != "P3")
 {
     throw "Bad magic number";
 }

 ppmFile >> width;
 if(width < 0 || width > MAX_WIDTH)
 {
     throw "Width out of bounds";
 }
 ppmFile >> height;
 if(height < 0 || height > MAX_HEIGHT)
 {
     throw "Height out of bounds";
 }
 ppmFile >> maxColorRange;
 if(maxColorRange != "255")
 {
     throw "Max color range not 255";
 }

         for(int y=0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                ppmFile>>image[y][x][RED];
                ppmFile>>image[y][x][GREEN];
                ppmFile>>image[y][x][BLUE];
                 if(image[y][x][RED] < 0 || image[y][x][RED] > 255 || image[y][x][GREEN] < 0 || image[y][x][GREEN] > 255 || image[y][x][BLUE] < 0 || image[y][x][BLUE] > 255)
                {
                    throw "Color value invalid";
                }
            }
        }

}

void ImageMaker::LoadImage(string filename) {

    ifstream ppmFile;
    ppmFile.open(filename);

    if(!ppmFile.is_open()) {        //If file can't be opened, throw an error
        throw "File failed to open";
    }

    string magicNum, maxColorRange;

    ppmFile >> magicNum;         //grab the first value from the ppm file (magic number)
    if(magicNum != "P3")        //If the magicNum in the file is not a p3 then throw an error
    {
        throw "Bad magic number";
    }

    ppmFile >> width;                     //grab the second value from the ppm file (the width)
    if(width <= 0 || width > MAX_WIDTH)  //if the width is 0, negative, or over the max limit, throw an error
    {
        throw "Width out of bounds";
    }

    ppmFile >> height;                      //grab the third value from the ppm file (the height)
    if(height <= 0 || height > MAX_HEIGHT)  //if the height is 0, negative, or over the max limit, throw an error
    {
        throw "Height out of bounds";
    }

    ppmFile >> maxColorRange;         //grab the fourth and final item in the ppmfile header, which is the max color range
    if(maxColorRange != "255")        //if its not 255, throw an error
    {
        throw "Max color range not 255";
    }

    for(int y=0; y<height; y++) {
        for (int x = 0; x < width; x++) {
            ppmFile>>image[y][x][RED];
            ppmFile>>image[y][x][GREEN];
            ppmFile>>image[y][x][BLUE];    //the hideous piece of code below checks to see if the color value is valid
            if(image[y][x][RED] < 0 || image[y][x][RED] > MAX_COLOR || image[y][x][GREEN] < 0 || image[y][x][GREEN] > MAX_COLOR || image[y][x][BLUE] < 0 || image[y][x][BLUE] > MAX_COLOR)
            {
                throw "Color value invalid";
            }
        }
    }
}

void ImageMaker::SaveImage(string filename) {
    ofstream ppmFile;
    ppmFile.open(filename);


    ppmFile << "P3" << endl;
    if(width <= 0 || width > MAX_WIDTH)
    {
        throw "Image must have non-zero dimensions";
    } else {
        ppmFile << width << " ";
    }
    if(height <= 0 || height > MAX_HEIGHT)
    {
        throw "Height out of bounds";
    } else {
        ppmFile << height << endl;
    }
    ppmFile << 255 << endl;

    for(int y=0; y<height; y++) {
        for (int x = 0; x < width; x++) {
            ppmFile<<image[y][x][RED] << " ";
            ppmFile<<image[y][x][GREEN] << " ";
            ppmFile<<image[y][x][BLUE] << " ";
        }
    }
}
// Size functions
int ImageMaker::GetWidth() const
{return width;}

int ImageMaker::GetHeight() const
{return height;}

void ImageMaker::SetWidth(int width)
{
    if (width < 0 || width > MAX_WIDTH) {
        throw "Width out of bounds";
    }
    else
    {
        this->width = width;
    }
}

void ImageMaker::SetHeight(int height)
{
    if (height < 0 || height > MAX_HEIGHT) {
        throw "Height out of bounds";
    }
    else
    {
        this->height = height;
    }
}

// Color functions
int ImageMaker::GetPenRed() const
{return pen_red;}

int ImageMaker::GetPenGreen() const
{return pen_green;}

int ImageMaker::GetPenBlue() const
{return pen_blue;}

void ImageMaker::SetPenRed(int newR)
{
    if(newR < 0 || newR > MAX_COLOR)
    {
        throw "Color value invalid";
    }
    pen_red = newR;
}

void ImageMaker::SetPenGreen(int newG)
    {
        if (newG < 0 || newG > MAX_COLOR) {
            throw "Color value invalid";
        }
        pen_green = newG;
    }

void ImageMaker::SetPenBlue(int newB)
{
    if (newB < 0 || newB > MAX_COLOR) {
        throw "Color value invalid";
    }
    pen_blue = newB;
}

void ImageMaker::DrawPixel(int x, int y)
{
    if(PointInBounds(x,y))
    {
        image[y][x][RED] = pen_red;    //Colors a pixel at the point (x,y)
        image[y][x][GREEN] = pen_green;
        image[y][x][BLUE] = pen_blue;
    }
    else {
        throw "Point out of bounds";
    }
}

void ImageMaker::DrawRectangle(int x1, int y1, int x2, int y2)
{
    int temp1, temp2;  //Temporary values in case the user enters a larger x1 and/or y1

    if(x1 > x2)         //If user gives x1 as the larger value
    {
        temp1 = x1;     //pass the larger value to a temp
        x1 = x2;        //Give the smaller number to x1 so the For loop works properly
        x2 = temp1;     //Give the larger value to x2 for the end position of the For loop
    }
    if(y1 > y2)         //If user gives y1 as the larger value
    {
        temp2 = y1;     //Pass the larger value to a temp
        y1 = y2;        //give the smaller value to y1 for the starting position of the For loop
        y2 = temp2;     //give the larger value to y2 for the ending position of the For loop
    }

    if(PointInBounds(x1,y1) && PointInBounds(x2,y2)) {

        if((x1 == y1) && (x2 == y2))
        {
            DrawPixel(x1,y1);
        }
        else {
            for (int x = x1; x <= x2; x++)          //Connect the dots from x1 to x2 to create one of the 4 rectangle edges
            {
                DrawPixel(x,y1);                    //Notice that y1 is constant so we are just drawing a horizontal line
            }
            for (int x = x1; x <= x2; x++) {        //This time the y2 is constant, drawing the 2nd of 4 rectangle edges
                DrawPixel(x,y2);
            }
            for (int y = y1; y <= y2; y++) {            //Now the x1 is constant, drawing a vertical line from y1 to y2
                DrawPixel(x1,y);
            }
            for (int y = y1; y <= y2; y++) {            //x2 is constant, this draws the fourth and final line (vertical)
                DrawPixel(x2,y);
            }
        }
    } else {
        throw "Point out of bounds";
    }
}

void ImageMaker::DrawLine(int x1, int y1, int x2, int y2)
{
    if(PointInBounds(x1,y1) && PointInBounds(x2,y2)){

        if((x1 == y1) && (x2 == y2)){
            DrawPixel(x1,y1);
        }

        if((x2 != x1)) {                                               //If the slope is not undefined

            float m = ((y2 - y1) / (x2 - x1));                       //calculate the slope and save as a float for possibility of decimals
            m = round(m);                                           // Round the decimal point if needed

            int m1 = m;                                            //save the rounded value as an integer because idk
            int y3;                                               //Make a new variable for the y values to help draw the line

            if(x2 > x1)                                          //Need this condition for the For loop to work properly
                {
                for (int x = x1; x <= x2; x++)                 //For loop goes through the x-values, starting at the smaller x value
                    {
                    y3 = m1 * x + round(y1-m1*x1);    //Linear equation will calculate each iteration to obtain new x and y3 values
                    DrawPixel(x,y3);

                    }
                }

            if(x1 > x2)              //Cannot just swap values in this situation because that will draw a different line
                {                   //So I just reverse the order of the For loop in the case of the user giving larger x1
                for (int x = x2; x <= x1; x++)  //Start the For loop at the smaller x value
                    {
                    y3 = m1 * x + round(y2-m1*x2);     //(50,200) ->  (200,50)
                    DrawPixel(x,y3);
                    }
                }
            }
        else  //if x1 = x2
            {

            if(y1 > y2){

            for(int y = y2; y<=y1; y++)    //If slope is undefined, user is attempting to draw a vertical line
                {
                DrawPixel(x1,y);
                }
            } else {
                for(int y=y1; y<=y2; y++){
                    DrawPixel(x2,y);
                }
            }
            }
    } else {
        throw "Point out of bounds";
    }
}

bool ImageMaker::PointInBounds(int x, int y) {
    if(x<0 || x >= width || y<0 || y >= height) {
        return false;
    } else {
        return true;
    }
}
