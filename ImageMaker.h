//
// Assignment Created by Varick Erickson
//
#ifndef IMAGEMAKER_H
#define IMAGEMAKER_H

#include <string>
#include <cmath>
#include <fstream>

const int MAX_WIDTH = 800;
const int MAX_HEIGHT = 800;
const int MAX_COLOR = 255;
enum COLOR { RED, GREEN, BLUE };

using namespace std;

//  Please put your pre and post comments in this file. See page 139 in the textbook.

class ImageMaker
{
public:
    ImageMaker();
    //Default Constructor.

    ImageMaker(string filename);
    //Constructor with a single argument.


    // Opens image with filename and stores information into
    void LoadImage(string filename);
    //Function: Loads an image, requires a ppm file name.
    //Pre: Ppm file exists, the magic number is P3, the width/height are between 0 and 800 and the Max color value is 255.
    //Post: if one of the conditions fails to be met, load will fail. Otherwise ppm image will load.

    void SaveImage(string filename);
    //Function: Saves an image as a ppm file.
    //Pre: The magic number must be P3, the width/height are between 0 and 800 and the Max color value is 255.
    //Post: If one of the preconditions fails to be met, save will fail. Otherwise ppm image will save.

    // Size functions
    int GetWidth() const;
    //Function: Determines the value of width.
    //Pre: width has been initialized.
    //Post: width value = number of pixels on the x-axis.

    int GetHeight() const;
    //Function: Determines the value of height.
    //Pre: height has been initialized.
    //Post: height value = number of pixels on the y-axis.

    void SetWidth(int width);
    //Function: Allows user to set the value of width.
    //Pre: width must be between 0 and 800 pixels
    //Post: If the precondition is met, width is set to the desired size.

    void SetHeight(int height);
    //Function: Allows user to set the value of height.
    //Pre: height must be between 0 and 800 pixels.
    //Post: If the precondition is met, height is set to the desired size.

    // Color functions
    int GetPenRed() const;
    //Function: Determines the value for the red pen.
    //Pre: red pen has been initialized.
    //Post: red pen value = RGB value between 0(black) and 255(white).

    int GetPenGreen() const;
    //Function: Determines the value for the green pen.
    //Pre: green pen has been initialized.
    //Post: green pen value = RGB value between 0(black) and 255(white).

    int GetPenBlue() const;
    //Function: Determines the value for the blue pen.
    //Pre: blue pen has been initialized.
    //Post: blue pen value = RGB value between 0(black) and 255(white).

    void SetPenRed(int newR);
    //Function: Allows the user to set the value of the red pen.
    //Pre: pen_red value must be between 0 and 255.
    //Post: if the precondition is met, red pen is set to the desired value.

    void SetPenGreen(int newG);
    //Function: Allows the user to set the value of green pen.
    //Pre: pen_green value must be between 0 and 255.
    //Post: if the precondition is met, green pen is set to the desired value.

    void SetPenBlue(int newB);
    //Function: Allows the user to set the value of blue pen.
    //Pre: pen_blue value must be between 0 and 255.
    //Post: if the precondition is met, blue pen is set to the desired value.

    // Drawing methods
    // These methods will use the current red, green, blue values of the pen
    void DrawPixel(int x, int y);
    //Function: Creates a pixel on a specific coordinate point (x,y)
    //Pre: the point (x,y) must in bounds, that is the value is between 0 and 800
    //Post: If the precondition is met, a pixel is drawn at location (x,y) in the color that the pens are set to.

    void DrawRectangle(int x1, int y1, int x2, int y2);
    //Function: Creates rectangles given 2 points on the x-y plane. (2 vertical lines and 2 horizontal lines)
    //Pre: the points (x1,y1), (x2,y2) must be in bounds (between 0 and 800).
    //Post: If the precondition is met, a rectangle is created with the given coordinate points.

    void DrawLine(int x1, int y1, int x2, int y2);
    //Function: Creates a line given 2 coordinate points on the x-y plane.
    //Pre: the points (x1,y1) and (x2,y2) must be in bounds (between 0 and 800).
    //Post: If the precondition is met, a line is drawn between (x1,y1) and (x2,y2).

private:
    int width;
    int height;
    int pen_red;    // Used by draw functions
    int pen_green;  // Used by draw functions
    int pen_blue;   // Used by draw functions
    bool PointInBounds(int x, int y);
    short image[MAX_WIDTH][MAX_HEIGHT][3];
};

#endif //IMAGEMAKER_H
